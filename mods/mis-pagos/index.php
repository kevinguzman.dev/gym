<?php
  require_once("../required/header.php"); 
  require("mispagos.php");
  $datos = MisPagos::getPagosAlumno($_SESSION["UserId"]);
  $datos = json_encode($datos);
?>

<section class="content-header">
  <h1>
    Mis pagos
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Mis pagos</li>
  </ol>
</section>

  <section class="content">
    <div class="box box-primary">
      
      <!-- /.box-header -->
      <div class="box-body">
        <table id="pagos" class="table table-striped responsive table-hover">
          <thead>
            <th>Fecha</th>
            <th>Monto</th>
            <th>Responsable</th>
            <th>Forma de pago</th>
          </thead>
        </table>
      </div>
    </div>
  </section>

<?php require_once("../required/footer.php");?>

<script type="text/javascript">
$(function () {
  $('#pagos').DataTable({
    'language'      : { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json" },
    'paging'        : true,
    'lengthChange'  : true,
    'searching'     : true,
    'ordering'      : true,
    'info'          : true,
    'autoWidth'     : true,
    'responsive'    : true,
    'data'          : <?= $datos ?>,
    'columns'       : [
                        { data: "creacion" },
                        { data: "monto" },
                        { data: "usuario" },
                        { data: "descripcion" }
                      ]
  });
});

</script>
<?php require_once("../required/scripts.php"); ?>