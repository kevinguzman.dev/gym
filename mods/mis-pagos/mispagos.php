<?php 

class MisPagos{

	public static function getPagosAlumno($id){
		$array		  = array();
		$con          = new connbd();
		$strconn      = $con->connect();

	    $sql          = "SELECT t1.codpago, t1.monto, t1.creacion, t2.nombre, t3.descripcion  
        			     FROM pagos t1 
        			     INNER JOIN usuarios t2 ON t1.usuario = t2.login
        			     INNER JOIN formaspago t3 ON t1.idformapago = t3.id
        				 WHERE codalumno = (SELECT id_alumno FROM usuarios WHERE codusuario = $id)";

		#--- ejecuta la query
		$res          = $strconn->query($sql) or die ("Error planes:" . mysqli_error($strconn));

		#--- se recojen los datos y se llena el array con los objetos 
		while($row = $res->fetch_assoc()){
			$obj 				= new stdClass();
			$obj->codPago 		= $row["codpago"];
			$obj->monto     	= dinero($row["monto"]);
		    $obj->creacion  	= dt($row["creacion"]);
		    $obj->usuario   	= $row["nombre"];
		    $obj->descripcion   = $row["descripcion"];
		   // $obj->creacion  = dbd($row["creacion"]); 
		    $array[]   		= $obj; 
		}

	    $strconn->close();    
	  	return $array;
	}


}
?>