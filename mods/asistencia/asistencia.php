<?php
class Asistencia{
  public $id;
  public $fecha;
  public $observaciones;
  public $usuario;
  public $codperiodo;
  public $alumnos;

  public function GetAll(){
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $sql          = "select id, fecha, t2.nombre as responsable, t3.nombre from asistencia t1 inner join usuarios t2 on t1.usuario = t2.login inner join periodos t3 on t1.codperiodo = t3.codperiodo";
    $res          = $strconn->query($sql) or die("Error get all: ".mysqli_error($strconn));
    $Gd_array     = array();

    if($res->num_rows >0):
      while($row = $res->fetch_assoc()):
          $obj = new stdClass();
          $obj->id          = $row["id"];
          $obj->fecha       = d($row["fecha"]);
          $obj->usuario     = $row["responsable"];
          $obj->nomperiodo  = $row["nombre"];
          $Gd_array[] = $obj;
      endwhile;
    endif;

    $strconn->close();
    return $Gd_array;
  }

  public function GetAlumnos(){
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $sql          = "select codalumno, nombre, rut from alumno where estado = 'A' and bjj = 1";
    $res          = $strconn->query($sql) or die("Error get alumnos: ".mysqli_error($strconn));
    $Gd_array     = array();

    if($res->num_rows > 0):
      while($row = $res->fetch_assoc()):
        $obj = new stdClass();
        $obj->id      = $row["codalumno"];
        $obj->rut     = $row["rut"];
        $obj->nombre  = $row["nombre"];

        $Gd_array[] = $obj;
      endwhile;
    endif;
    $strconn->close();
    return $Gd_array;
  }

  public function Save(){
    $conn        = new connbd();
    $strconn     = $conn->connect();
    $Gd_msj      = "";

    $Gd_fecha    = FechaBD($this->fecha);
    #---inserta cabecera de la asistencia
    $sql   = "INSERT INTO asistencia (fecha, observaciones, usuario, codperiodo)";
    $sql  .= "values('".$Gd_fecha."', '".$this->observaciones."', '".$this->usuario."', ".$this->codperiodo.")";

    $res  = $strconn->query($sql) or die("Error insert cabecera: ".mysqli_error($strconn));

    #--- verifica si se insertó correctamente, si no detiene la depuración
    if(mysqli_affected_rows($strconn) > 0):
      $Gd_msj = "OK";
    else:
      $Gd_msj = mysqli_error($strconn);
      exit;
    endif;

    $this->id = $strconn->insert_id;

    #--- inserta el detalle de la asistencia
    for($i = 0; $i < count($this->alumnos); $i++):
      $sql  = "INSERT INTO asistencia_alumno (id_alumno, id_asistencia)";
      $sql .= "VALUES(".$this->alumnos[$i].", ".$this->id.")";
      $res  = $strconn->query($sql) or die("Error insert detalle: ".mysqli_error($strconn));
    endfor;

    #--- verifica si se insertó correctamente, si no detiene la depuración
    if(mysqli_affected_rows($strconn) > 0):
      $Gd_msj = "OK";
    else:
      $Gd_msj = mysqli_error($strconn);
      exit;
    endif;

    $strconn->close();

    return $Gd_msj;
  }

  public function GetAsistencia($id){
    $conn        = new connbd();
    $strconn     = $conn->connect();
    $Gd_obj      = new Asistencia();

    #--- obtiene la cabecera de la asistencia
    $sql         = "SELECT fecha, observaciones, t2.nombre FROM asistencia t1 INNER JOIN usuarios t2 ON t1.usuario = t2.login WHERE t1.id = ".$id;
    $res         = $strconn->query($sql) or die (mysqli_error($strconn));

    if($res->num_rows > 0):
      $row                    = $res->fetch_assoc();
      $Gd_obj->fecha          = BDFecha($row["fecha"]);
      $Gd_obj->observaciones  = $row["observaciones"];
      $Gd_obj->usuario        = $row["nombre"];
    endif;

    #--- obtiene los alumnos asociados a la asistencia
    $sql        = "SELECT t1.id_alumno, nombre, rut from asistencia_alumno t1 inner join alumno t2 on t1.id_alumno = t2.codalumno where t1.id_asistencia = ".$id;
    $res        = $strconn->query($sql) or die ("Error traer alumnos: ". mysqli_error($strconn) );

    if($res->num_rows > 0):
      while($row = $res->fetch_assoc()):
        $obj = new stdClass();
        $obj->nombre      = $row["nombre"];
        $obj->rut         = $row["rut"];

        $Gd_obj->alumnos[] = $obj;
      endwhile;
    endif;

    $strconn->close();
    return $Gd_obj;
  }
}
?>
