<?php
session_start();
header("Content-type: text/html; charset=utf-8");
require_once("../required/functions.php");
require_once("../required/connbd.php");
require_once("asistencia.php");

if(isset($_POST) && isset($_POST["fecha"]) && isset($_POST["accion"]) && $_POST["accion"] == "ADD"):
  $Gd_alumnos = [];

  if(isset($_POST["alumnos"])):
    $Gd_alumnos = $_POST["alumnos"];
  endif;

  $Gd_asistencia                  = new Asistencia();
  $Gd_asistencia->fecha           = $_POST["fecha"];
  $Gd_asistencia->observaciones   = $_POST["observaciones"];
  $Gd_asistencia->codperiodo      = $_POST["codperiodo"];
  $Gd_asistencia->usuario         = $_SESSION["Login"];
  $Gd_asistencia->alumnos         = $Gd_alumnos;

  echo $Gd_asistencia->Save();

endif;
?>
