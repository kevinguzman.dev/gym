<?php
class Periodo{
  public $codigo;
  public $nombre;
  public $estado;

  function __construct(){}

  function GetAll(){
    //conexión bd
    $conn        = new connbd();
    $strconn     = $conn->connect();

    //traer periodos
    $sql        = "SELECT codperiodo, nombre, estado FROM periodos ORDER BY codperiodo DESC";

    $resultado  = $strconn->query($sql) or die ("Error: " . mysqli_error($conn));
    $Gd_array   = array();

    if($resultado->num_rows > 0):
      while($row = $resultado->fetch_assoc()):
        $per = new Periodo();

        $per->codigo  = $row["codperiodo"];
        $per->nombre  = $row["nombre"];
        $per->estado  = estadoPeriodo($row["estado"]);

        $Gd_array[]   = $per;
      endwhile;
    endif;

    $strconn->close();
    return $Gd_array;
  }

  function Save(){
    //conexión bd
    $conn     = new connbd();
    $strconn  = $conn->connect();
    $sql      = "";

    if(!$this->Exists()):
        $sql = "INSERT INTO periodos(nombre, estado) values ('".$this->nombre."', '".$this->estado."')";
        $res = $strconn->query($sql)  or die("Error insert: ".mysqli_error($strconn));
        $this->codigo = $strconn->insert_id;
    else:
        $sql  = "UPDATE periodos SET ";
        $sql .= "Nombre = '".$this->nombre."',";
        $sql .= "Estado = '".$this->estado."'";
        $sql .= "WHERE codperiodo = ".$this->codigo;
        $res = $strconn->query($sql) or die("Error update: ".mysqli_error($strconn));
    endif;

    $this->Get($this->codigo);

  }

  static function ExistsOpened(){
    $conn         = new connbd();
    $strconn      = $conn->connect();

    $sql = "select count(codperiodo) as cont from periodos where estado = 'A'";
    $res = $strconn->query($sql) or die ("Error: ".mysqli_error($strconn));

    if($res->num_rows > 0):
      $row = $res->fetch_assoc();
      if($row["cont"] == 0):
        return false;
      else:
        return true;
      endif;
    endif;
  }

  function Exists(){
    $conn         = new connbd();
    $strconn      = $conn->connect();

    $sqlExist     =  "select count(codperiodo) as cont from periodos where codperiodo = ".$this->codigo." LIMIT 1";
    $resultado    = $strconn->query($sqlExist) or die("Error ".mysqli_error($strconn));

    if($resultado->num_rows > 0):
      $row = $resultado->fetch_assoc();
      if($row["cont"] > 0):
        return true;
      else:
        return false;
      endif;
    endif;
  }

  function Get($id){
    $conn     = new connbd();
    $strconn  = $conn->connect();

    $sql      = "SELECT codperiodo, nombre, estado FROM periodos WHERE codperiodo = ".$id." LIMIT 1";
    $res      = $strconn->query($sql) or die("Error: ".mysqli_error($conn));

    $Gd_per   = new Periodo();

    if($res->num_rows > 0):
  		$row      = $res->fetch_assoc();
  		$Gd_per->codigo = $row["codperiodo"];
  		$Gd_per->nombre = $row["nombre"];
  		$Gd_per->estado = $row["estado"];
    endif;

    $strconn->close();

    return $Gd_per;
  }

  #--- muestra los pagos asociados a un periodo
  function GetPagos($id){
	   $conn     	= new connbd();
     $strconn  	= $conn->connect();

    $sql 		    = "select codpago, t1.creacion, monto, observacion, t2.nombre, t3.nombre as usuario  ";
    $sql	     .= "from pagos t1 ";
    $sql	     .= "inner join alumno t2 on t1.codalumno = t2.codalumno ";
    $sql       .= "inner join usuarios t3 on t1.usuario = t3.login ";
    $sql	     .= "where t1.codperiodo = ".$id." AND t1.estado = 'A' ";
    $sql       .= "order by codpago desc";
    
  	$res		= $strconn->query($sql) or die("Error: " . mysqli_error($strconn));
  	$array		= array();
  	if($res->num_rows> 0):
  		while($row = $res->fetch_assoc()):
  			$obj = new stdClass();

  			$obj->codpago 		   = $row["codpago"];
  			$obj->creacion 		   = dt($row["creacion"]);
  			$obj->monto 		     = dinero($row["monto"]);
  			$obj->observacion	   = $row["observacion"];
  			$obj->alumno 		     = $row["nombre"];
        $obj->responsable    = $row["usuario"];
  			$array[] = $obj;
  		endwhile;
  	endif;

  	$strconn->close();
  	return $array;
  }

  #--- muestra los gastos asociados a un período
  function GetGastos($id){
    $conn     	= new connbd();
    $strconn  	= $conn->connect();

    $sql = "select codgasto, descripcion, fecha, monto, t3.nombre as responsable " ;
    $sql .= "from gastos t1 inner join periodos t2 on t1.codperiodo = t2.codperiodo inner join usuarios t3 on t1.responsable = t3.login ";
    $sql .= "where t1.codperiodo = ".$id." order by codgasto desc";

    $res = $strconn->query($sql) or die("Error: " . mysqli_error($strconn));
    $Gd_array = array();
    if($res->num_rows > 0):
      while($row = $res->fetch_assoc()):
        $obj = new stdClass();

        $obj->codgasto = $row["codgasto"];
        $obj->descripcion = $row["descripcion"];
        $obj->fecha = dt($row["fecha"]);
        $obj->monto = dinero($row["monto"]);
        $obj->responsable = $row["responsable"];

        $Gd_array[] = $obj;
      endwhile;

      $strconn->close();

      return $Gd_array;
    endif;
  }

  function GetImpagos($id){
    $conn     	= new connbd();
    $strconn  	= $conn->connect();

    $sql        = " SELECT t1.codalumno, t1.nombre, t1.rut, t1.becado, t2.nombre as nomPeriodo
                    FROM alumno t1 
                    INNER JOIN periodos t2 ON t1.codultperiodopagado = t2.codperiodo
                    WHERE 
                      codultperiodopagado < 23 
                      AND (t1.becado is null or t1.becado = 0)
                    ORDER BY t1.codultperiodopagado DESC";
  
    $res = $strconn->query($sql) or die("Error: " . mysqli_error($strconn));
    $Gd_array = array();

    if($res->num_rows > 0):
      while($row = $res->fetch_assoc()):
        $obj = new stdClass();

        $obj->codAlumno   = $row["codalumno"];
        $obj->nombre      = $row["nombre"];
        $obj->rut         = $row["rut"];
        $obj->ultPeriodo  = $row["nomPeriodo"];

        $Gd_array[]       = $obj;
      endwhile;
    endif;

    $strconn->close();
    return $Gd_array;
  }

  function SetEstado($id){
    $conn     	= new connbd();
    $strconn  	= $conn->connect();
    $Gd_msj     = "";

    $sql = "UPDATE periodos SET estado = 'C' WHERE codperiodo = ".$id;
    $res = $strconn->query($sql);

    if(mysqli_affected_rows($strconn) > 0):
      $Gd_msj = "OK";
    else:
      $Gd_msj = mysqli_error($strconn);
      exit;
    endif;
    $strconn->close();
    return $Gd_msj;
  }

  public function getUltimoPeriodoCerrado(){
    $conn     	= new connbd();
    $strconn  	= $conn->connect();
    $codPeriodo = 0;

    $sql        = " select codperiodo 
                    from periodos 
                    where codperiodo = (select max(t1.codperiodo) 
                                        from periodos t1
                                        inner join pagos t2 ON t1.codperiodo = t2.codperiodo
                                        where t1.estado = 'C') ";

    $res = $strconn->query($sql) or die("Error: " . mysqli_error($strconn));

    if($res->num_rows > 0):
      $row  = $res->fetch_assoc();

      $codPeriodo = $row["codperiodo"];
    endif;

    $strconn->close();

    return $codPeriodo;
  }

}


 ?>
