<?php
require_once("../required/header.php");
require_once("periodo.php");
$Gd_accion          = "ADD";
$Gd_codigo          = 0;
$Gd_nombre          = nombrePeriodo();
$Gd_estado          = "";
$Gd_existePAbierto  = Periodo::ExistsOpened();

if(isset($_GET["id"]) && $_GET["id"] > 0):
  $Gd_id  = $_GET["id"];
  $Gd_per = new Periodo();
  $Gd_per = $Gd_per->Get($Gd_id);

  $Gd_codigo  = $Gd_per->codigo;
  $Gd_nombre  = $Gd_per->nombre;
  $Gd_estado  = $Gd_per->estado;
endif;

if(isset($_POST["submit"])):
  $Gd_nombre = $_POST["nombre"];
  $Gd_estado = $_POST["estado"];
  $Gd_codigo = $_POST["codigo"];

  $Gd_per = new Periodo();
  $Gd_per->nombre = $Gd_nombre;
  $Gd_per->estado = $Gd_estado;
  $Gd_per->codigo = $Gd_codigo;

  $Gd_per->Save();
  $Gd_codigo = $Gd_per->codigo;
  $Gd_estado = $Gd_per->estado;
endif;
?>

<section class="content-header">
  <h1>
    <?php if($Gd_accion == "ADD"): ?>
      Crear
    <?php else: ?>
      Modificar
    <?php endif; ?>
    Periodo
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=$Gl_appUrl?>/periodos/index">Periodos</a></li>
    <li class="active">
      <?php if($Gd_accion == "ADD"): ?>
        Crear
      <?php else: ?>
        Modificar
      <?php endif; ?>
    </li>
  </ol>
</section>

<section class="content">
  <?php if($Gd_existePAbierto): ?>
  <div class="callout callout-danger">
    <h4>
       Estimad@ Usuari@:
    </h4>
     <p>
      Ya existe un periodo abierto, favor cerrarlo para poder crear uno nuevo.
    </p>
   </div>
 <?php endif;?>
  <div class="row">
    <div class="col-xs-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#periodo" data-toggle="tab">Periodo</a></li>
        </ul>

        <div class="tab-content">
          <div class="tab-pane active" id="alumno">
            <form role="form" action="<?= $Gl_appUrl ?>/periodos/form/<?=$Gd_codigo?>" method="POST">
              <input type="hidden" name="codigo" value="<?= $Gd_codigo ?>" id="codigo">
              <div class="box-body">
                <div class="form-group">
                  <label for="Nombre">Nombre</label>
                  <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingrese nombre" value="<?= $Gd_nombre ?>">
                </div>

                <div class="form-group">
                  <label for="estado">Estado</label>
                  <select class="form-control" name="estado" id="estado">
                    <option value="A" <?php if($Gd_estado == "A"): ?> selected=""<?php endif; ?>>Abierto</option>
                    <option value="C" <?php if($Gd_estado == "C"): ?> selected=""<?php endif; ?>>Cerrado</option>
                  </select>
                </div>
              </div>
              <div class="box-footer">
                <?php
                  $Gd_disabled = '';
                  if($Gd_existePAbierto or $Gd_estado == 'C'):
                    $Gd_disabled = 'disabled=""';
                  endif;
                 ?>
                <button type="button" name="submit" class="btn btn-primary" <?= $Gd_disabled ?> id="boton">Guardar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
<?php require_once("../required/footer.php"); ?>
<script type="text/javascript">
$(function(){
  $("#boton").click(function(){
    AlertConfirm("Confirmación", "Desea guardar los cambios realizados?", function(res){
      if(res){
        Load("Cargando...");

        var json        = new Object();
        json["codigo"]  = $("#codigo").val();
        json["nombre"]  = $("#nombre").val();
        json["estado"]  = $("#estado").val();
        json["accion"]  = "PER";

        $.ajax({
          url: "<?=$Gl_appUrl?>/periodos/ajax",
          type: 'POST',
          dataType: 'json',
          data: json,
          success: function(data) {
            AlertSuccess("Éxito", "Período creado con éxito", "<?=$Gl_appUrl?>/periodos/index");
          },
          error: function(data){
            AlertError("Error", data.responseText);
          }
        });
        
      }
    });
  });
});
</script>
<?php require_once("../required/scripts.php"); ?>
