<?php
	require_once("../required/header.php");
	require_once("periodo.php");

	$Gd_id 				= $_GET["id"];
	$per          = new Periodo();
	$Gd_pagos  		= json_encode($per->GetPagos($Gd_id));
	$Gd_cPagos		= $per->Get($Gd_id);
?>

<section class="content-header">
  <h1>
    Pagos
    <small>por período</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=$Gl_appUrl?>/periodos/index">Periodos</a></li>
		<li class="active">Pagos por período</li>
  </ol>
</section>

<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Listado de todos los pagos asociados al periodo: <b><?= $Gd_cPagos->nombre ?> </b></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="periodos" class="table table-bordered table-striped">
        <thead>
          <th>Creación</th>
          <th>Monto</th>
          <th>Observación</th>
          <th>Alumno</th>
					<th>Responsable</th>
        </thead>
      </table>
    </div>
  </div>
</section>

<?php require_once("../required/footer.php");?>
<script type="text/javascript">
$(function () {
  $('#periodos').DataTable({
    'language'      : { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json" },
    'paging'        : true,
    'lengthChange'  : true,
    'searching'     : true,
    'ordering'      : true,
    'info'          : true,
    'autoWidth'     : false,
    'data'          : <?= $Gd_pagos ?>,
    'columns'       : [
                        { data: "creacion" },
                        { data: "monto" },
                        { data: "observacion" },
                        { data: "alumno" },
												{ data: "responsable" },
                      ]
  })
})
</script>
<?php require_once("../required/scripts.php"); ?>
