<?php
	require_once("../required/header.php");
	require_once("periodo.php");

	$Gd_id 	 = $_GET["id"];
	$per     = new Periodo();
  $Gd_per  = $per->Get($Gd_id);
  $impagos = json_encode($per->GetImpagos($Gd_id));
?>

<section class="content-header">
  <h1>
    Alumnos impagos
    <small>por período</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=$Gl_appUrl?>/periodos/index">Periodos</a></li>
		<li class="active">Alumnos impagos</li>
  </ol>
</section>

<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Listado de los morosos asociados al período: <b><?= $Gd_per->nombre ?> </b></h3>
      <div class="box-tools">
        <a href="<?=$Gl_appUrl?>/pagos/form" class="btn btn-default">Registrar pago</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="periodos" class="table table-bordered table-striped">
        <thead>
          <th>Alumno</th>
        </thead>
      </table>
    </div>
  </div>
</section>



<?php require_once("../required/footer.php");?>
<script type="text/javascript">
$(function () {
  $('#periodos').DataTable({
    'language'      : { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json" },
    'paging'        : true,
    'lengthChange'  : true,
    'searching'     : true,
    'ordering'      : true,
    'info'          : true,
    'autoWidth'     : false,
    'data'          : <?= $impagos ?>,
    'columns'       : [
                        { data: "nombre" },
                      ]
  })
})
</script>
<?php require_once("../required/scripts.php"); ?>
