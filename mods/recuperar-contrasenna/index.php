<?php 
require_once("../required/connbd.php");
require_once("../required/functions.php");
require_once("recuperador.php");

$Gd_tipoMsj   = "danger";  
$Gd_msj       = "";
$Gd_resp      = false;
$Gd_json      = json_decode(file_get_contents("../required/config.json"));
$Gl_appName   = $Gd_json->{"appName"};
$Gl_appUrl    = $Gd_json->{"appUrl"};

if(isset($_POST["email"]) && $_POST["email"] != ""):
  if(Recuperador::Recuperar($_POST["email"])):
    $Gd_resp    = true;
    $Gd_msj     = "Se ha enviado un correo electrónico con la nueva contraseña, favor seguir las instrucciones indicadas en el mensaje.";
    $Gd_tipoMsj = "success";
  else:
    $Gd_msj  = "No se encuentran usuarios asociados al mail indicado.";
  endif;
elseif(isset($_POST["email"]) && $_POST["email"]===""):
  $Gd_msj   = "Debes ingresar tu correo para poder recuperar tu contraseña" ;
endif;

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>kGym | Recuperar contraseña</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/plugins/iCheck/square/blue.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style>
  body{
    background-image: url("../img/sys/bg.jpg");
  }
</style>

<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="#"><b>k</b>GYM </a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <?php 
        if($Gd_msj != ""):         
        ?>
      <div class="alert alert-<?= $Gd_tipoMsj ?> fadeIn">
        <?= $Gd_msj ?>
      </div>
      <?php endif; ?>

      <p class="login-box-msg">Ingresa tu email para recuperar tu contraseña:</p>

      <form action="#" method="post">
        <div class="form-group has-feedback">
          <input type="email" class="form-control" placeholder="Email" name="email" id="email" autocomplete="off">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>

        <div class="row">
          <div class="col-xs-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Recuperar</button>
          </div>
        </div>
      </form>

      <div class="social-auth-links text-center">
        <a href="<?= $Gl_appUrl ?>/login/index">Iniciar sesión</a><br>
      </div>
    </div>
    <!-- /.login-box-body -->
  </div>

  <script src="<?=$Gl_appUrl ?>/mods/template/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="<?=$Gl_appUrl ?>/mods/template/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
