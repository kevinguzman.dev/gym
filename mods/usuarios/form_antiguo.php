<?php
require_once("../required/header.php");
require_once("usuario.php");
#------------------
#---- Variables----
#------------------
$Gd_codusuario = "";
$Gd_nombre     = "";
$Gd_estado     = "";
$Gd_foto       = "";
$Gd_login      = "";
$Gd_contrasena = "";
$Gd_accion     = "ADD";
$Gd_exito      = false;
$Gd_perfiles   = Usuario::GetPerfiles();

if( isset($_POST["codigo"]) ):
  $Gd_codusuario = $_POST["codigo"];
  $Gd_nombre     = $_POST["nombre"];
  $Gd_estado     = $_POST["estado"];
  $Gd_login      = $_POST["username"];
  $Gd_contrasena = $_POST["contrasena"];

  $obj = new Usuario();
  $obj->codusuario  = $Gd_codusuario;
  $obj->nombre      = $Gd_nombre;
  $obj->estado      = $Gd_estado;
  $obj->login       = $Gd_login;
  $obj->contrasena  = $Gd_contrasena;
  $obj->foto        = "no-photo.png";

  $obj->Save();
  $Gd_exito = true;
endif;

if( isset($_GET["id"]) and $_GET["id"] > 0):
  $Gd_accion  = "GET";
  $Gd_codigo  = $_GET["id"];

  $obj = new Usuario();
  $obj = $obj->Get($Gd_codigo);

  $Gd_codusuario = $Gd_codigo;
  $Gd_nombre     = $obj->nombre;
  $Gd_estado     = $obj->estado;
  $Gd_login      = $obj->login;
  $Gd_contrasena = $obj->contrasena;
endif;
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php if($Gd_accion == "ADD"): ?>
          Crear
        <?php else: ?>
          Modificar
        <?php endif; ?>
        Usuario
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?=$Gl_appUrl?>/alumnos/index">Alumnos</a></li>
        <li class="active">
          <?php if($Gd_accion == "ADD"): ?>
            Crear
          <?php else: ?>
            Modificar
          <?php endif; ?>
        </li>
      </ol>
    </section>

    <section class="content">

      <div class="row">
        <div class="col-xs-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#alumno" data-toggle="tab">Usuarios</a></li>
              <li class="dropdown pull-right">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  Opciones <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= $Gl_appUrl ?>/usuarios/index">Ver todos</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= $Gl_appUrl ?>/usuarios/form">Registrar nuevo</a></li>
                </ul>
              </li>
            </ul>

            <div class="tab-content">
              <div class="tab-pane active" id="alumno">
                <form role="form" action="<?= $Gl_appUrl ?>/usuarios/form/<?=$Gd_codusuario?>" method="POST">
                  <input type="hidden" name="codigo" value="<?= $Gd_codusuario ?>">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="Nombre">Nombre</label>
                      <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingrese nombre del usuario" value="<?= $Gd_nombre ?>" autocomplete="off" required="">
                    </div>

                    <div class="form-group">
                      <label for="usuario">Usuario</label>
                      <input type="text" name="username" class="form-control" id="username" placeholder="Ingrese el nombre de usuario con el que se autenticará en el sistema" value="<?= $Gd_login ?>" autocomplete="off" required="">
                    </div>

                    <div class="form-group dvContrasena">
                      <label for="contrasena">Contraseña</label>
                      <input type="password" name="contrasena" class="form-control" id="contrasena" placeholder="Ingrese contraseña del usuario" value="<?= $Gd_contrasena ?>" required="">
                    </div>

                    <div class="form-group dvContrasena">
                      <label for="contrasena2">Confirme contraseña</label>
                      <input type="password" name="contrasena2" class="form-control" id="contrasena2" placeholder="Reingrese contraseña del usuario" value="<?= $Gd_contrasena ?>" required="" onchange="compararContrasenas();">
                      <span class="help-block" id="msjError" style="display:none;">Contraseñas no coinciden</span>
                    </div>

                    <div class="form-group" id="divFoto" style="display:none;">
                      <label for="foto">Foto</label>
                      <input type="file" class="form-control" id="foto" name="foto" value="<?= $Gd_foto ?>">
                      <span class="help-block" id="msjErrorFoto" style="display:none;">Verifique extensión del archivo</span>
                    </div>

                    <div class="form-group">
                      <img id="preFoto" src="#" alt="foto" style="display:none;" width="250px" height="250px" class="img img-responsive img-circle"/>
                    </div>

                    <div class="form-group">
                      <label for="estado">Estado</label>
                      <select class="form-control" name="estado" id="estado" style="width: 100%;">
                          <?php
                            $Gd_select = "";
                            if($Gd_estado == "A" || $Gd_estado == "I"):
                              $Gd_select = "selected=''";
                            endif;
                          ?>
                          <option>---</option>
                          <option value="A" <?= $Gd_select?>>Activo</option>
                          <option value="I" <?= $Gd_select?>>Inactivo</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                      <label for="estado">Perfil</label>
                      <select class="form-control" name="estado" id="estado" style="width: 100%;">
                        <?php for($i = 0; $i < count($Gd_perfiles); $i++):?>
                            <option value=<?=$Gd_perfiles[$i]->id?>><?=$Gd_perfiles[$i]->nombre?></option>
                          <?php endfor;?>
                      </select>
                    </div>
                  </div>

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" id="btn">Guardar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div> <!-- col-xs-12 -->
      </div> <!-- row -->

    </section>
<?php require_once("../required/footer.php"); ?>
<script type="text/javascript">
    compararContrasenas = function(){
        var con1 = $("#contrasena").val();
        var con2 = $("#contrasena2").val();

        if(con1 != con2){
          $(".dvContrasena").addClass("has-error");
          $("#msjError").removeAttr("style");
          $("#btn").attr("disabled", "disabled");
        }else{
          $("#msjError").hide();
          $(".dvContrasena").removeClass("has-error");
          $("#btn").removeAttr("disabled");
        }
    }

    $("#foto").change(function(){
      var ext     = this.value.match(/\.(.+)$/)[1];
      ext.toString().toUpperCase();
      console.log(ext);
      var valida  = true;

      switch (ext) {
          case 'JPG':
            valida = true;
            break;
          case 'JPEG':
            valida = true;
            break;
          case 'PNG':
            valida = true;
            break;
          default:
              valida = false;
              break;
      }

      if(valida){
        readUrl(this);
        $("#msjErrorFoto").hide();
        $("#divFoto").removeClass("has-error");
      }else{
        $("#divFoto").addClass("has-error");
        $("#msjErrorFoto").removeAttr("style");
      }
    });

    readUrl = function (input) {
        if (input.files && input.files[0]) {
            $("#preFoto").removeAttr("style");
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#preFoto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


  }
</script>
<?php require_once("../required/scripts.php"); ?>
