<?php
class Usuario{
  public $codusuario;
  public $nombre;
  public $creacion;
  public $estado;
  public $foto;
  public $login;
  public $contrasena;
  public $perfil;

  public function GetAll(){
    //conexión bd
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $sql      = "select codusuario, t1.creacion, password, t1.nombre, t1.estado, foto, t1.login, t2.nombre as nomperfil ";
    $sql     .= "from usuarios t1 ";
    $sql     .= "inner join perfil t2 on t1.id_perfil = t2.id";

    $res      = $strconn->query($sql) or die (mysqli_error($strconn));
    $Gd_array = array();

    if($res->num_rows > 0):
      while($row = $res->fetch_assoc()):
        $user = new Usuario();
        $user->codusuario   = $row["codusuario"];
        $user->nombre       = $row["nombre"];
        $user->creacion     = dt($row["creacion"]);
        $user->estado       = $row["estado"];
        $user->foto         = $row["foto"];
        $user->login        = $row["login"];
        $user->perfil       = $row["nomperfil"];
        
        $Gd_array[] = $user;
      endwhile;
    endif;

    $strconn->close();

    return $Gd_array;
  }

  public function Get($id){
    //conexión bd
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $sql      = "select codusuario, creacion, password, nombre, estado, foto, login, password, id_perfil from usuarios where codusuario = ".$id." limit 1";
    $res      = $strconn->query($sql) or die (mysqli_error($strconn));
    $user     = new Usuario();

    if($res->num_rows > 0):
      $row = $res->fetch_assoc();

      $user->codusuario   = $row["codusuario"];
      $user->nombre       = $row["nombre"];
      $user->creacion     = dt($row["creacion"]);
      $user->estado       = $row["estado"];
      $user->foto         = $row["foto"];
      $user->login        = $row["login"];
      $user->contrasena   = $row["password"];
      $user->perfil       = $row["id_perfil"];
    endif;

    $strconn->close();
    return $user;
  }


  public function Save(){
    //conexión bd
    $conn        = new connbd();
    $strconn     = $conn->connect();

    if($this->Exists($this->codusuario)):
      #--- lo modifica
      $sql  = "UPDATE usuarios SET ";
      $sql .= "nombre     = '".$this->nombre."', ";
      $sql .= "password   = '".$this->contrasena."', ";
      $sql .= "foto       = '".$this->foto."', ";
      $sql .= "login      = '".$this->login."', ";
      $sql .= "id_perfil  = '".$this->perfil."' ";
      if($this->codusuario == ""):
        $sql .= "WHERE login = '".$this->login."' ";
      else:
        $sql .= "WHERE codusuario = ".$this->codusuario;
      endif;

      $res = $strconn->query($sql) or die("error update: " . mysqli_error($strconn) );
    else:
      #--- inserta
      $sql = "INSERT INTO usuarios (nombre, password, estado, foto, login, creacion, id_perfil)";
      $sql .= "values( '".$this->nombre."', '".$this->contrasena."', 'A', '".$this->foto."', '".$this->login."', now(), ".$this->perfil.")";

      $res = $strconn->query($sql) or die ("Error insert: " . mysqli_error($strconn));
      $this->codusuario = $strconn->insert_id;
    endif;

    $strconn->close();
  }

  public function Exists($id){
    //conexión bd
    $conn        = new connbd();
    $strconn     = $conn->connect();
    if($id == ""):
      $sql = "SELECT count(codusuario) as cont FROM usuarios where login = '".$this->login."'";
    else:
      $sql = "SELECT count(codusuario) as cont FROM usuarios where codusuario = ".$id;
    endif;
    $res = $strconn->query($sql) or die("Error exists: " . mysqli_error($strconn));

    if($res->num_rows > 0):
      $row = $res->fetch_assoc();

      if($row["cont"] > 0):
        return true;
      else:
        return false;
      endif;

    endif;
  }

  public static function GetPerfiles(){
    //conexión bd
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $sql         = "SELECT id, nombre FROM perfil";
    $res         = $strconn->query($sql) or die ("Error perfiles: " . mysqli_error($strconn));
    $array       = array();
    
    if($res->num_rows > 0):
      while($row = $res->fetch_assoc()):
        $obj          = new stdClass();
        $obj->id      = $row["id"];
        $obj->nombre  = $row["nombre"];
        $array[]      = $obj;
      endwhile;
    endif;

    $strconn->close();
    return $array;
  }

  public function CambiarEstado(){
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $this->estado     = clear($this->estado, $strconn);
    $this->codusuario = clear($this->codusuario, $strconn);
    $sql              = "UPDATE usuarios SET estado = '".$this->estado."' WHERE codusuario = ".$this->codusuario;
    pre( $sql );
    $strconn->query($sql) or die("Error cambia estado: " . mysqli_error($strconn));
    $strconn->close();
  }
}


 ?>
