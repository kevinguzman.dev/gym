<?php
  require_once("../required/header.php");
  require_once("dashboard.php");
  require_once("../pagos/pago.php");
  require_once("../alumnos/alumno.php");

  $Gd_obj           = new Dashboard();
  $Gd_data          = $Gd_obj->GetData();

  $Gd_graph         = $Gd_obj->GetGraphData();
  $Gd_periodos      = json_encode($Gd_graph[0]->periodos);
  $Gd_pagos         = json_encode($Gd_graph[0]->pagos);
  $Gd_gastos        = json_encode($Gd_graph[0]->gastos);

  #--- obtiene los datos de la clase pago
  $Gd_pago          = new Pago();
  $Gd_listaPagos    = $Gd_pago->GetAll(true);

  #--- obtiene los datos de los usuarios registrados
  $Gd_alumno        = new Alumno();
  $Gd_listaAlumnos  = $Gd_alumno->GetAll(true);
?>

<?php 

#--- dashboard para perfil administrador
switch($_SESSION["Perfil"]){
  case 1:
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Periodo actual:
    <small><?= $_SESSION["nomperiodo"] ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Escritorio</li>
  </ol>
</section>


  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-green">
          <div class="inner">
            <h3><?= $Gd_data->totalPagos?></h3>
            <p>Pagos registrados</p>
          </div>
          <div class="icon">
            <i class="ion ion-cash"></i>
          </div>
          <a href="<?=$Gl_appUrl?>/periodos/pagos/<?=$Gd_data->idPeriodo?>" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
          <div class="inner">
            <h3><?= $Gd_data->totalGastos ?></h3>
            <p>Gastos registrados
          </div>
          <div class="icon">
            <i class="ion ion-ios-cart-outline"></i>
          </div>
          <a href="<?=$Gl_appUrl?>/periodos/gastos/<?=$Gd_data->idPeriodo?>" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3><?= $Gd_data->totalAlumnos?></h3>
            <p>Nuevos alumnos</p>
          </div>
          <div class="icon">
            <i class="ion ion-person-add"></i>
          </div>
          <a href="<?=$Gl_appUrl?>/alumnos/index" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3><?=$Gd_data->promAsistencia?></h3>

            <p>Promedio de asistencia</p>
          </div>
          <div class="icon">
            <i class="ion ion-university"></i>
          </div>
          <a href="<?=$Gl_appUrl?>/asistencia/index" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
    </div>
    
    <div class="row">
      <section class="col-lg-12 connectedSortable">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs pull-right">
            <li class="pull-left header"><i class="fa fa-inbox"></i> Pagos vs Gastos</li>
          </ul>
          <div class="tab-content no-padding">
            <canvas id="grafico" style="height: 300px !important; margin-top: 20px;"></canvas>
          </div>
        </div>
      </section>
    </div>

    <div class="row">
      <div class="col-md-4">
        <div class="box box-info">

          <div class="box-header with-border">
            <h3 class="box-title">Alumnos con mayor asistencia</h3>
          </div>

          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Total clases</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>---</td>
                    <td>---</td>
                    <td>---</td>
                  </tr>
                  <tr>
                    <td>---</td>  
                    <td>---</td>
                    <td>---</td>
                  </tr>
                  <tr>
                    <td>---</td>
                    <td>---</td>
                    <td>---</td>
                  </tr>
                  <tr>
                    <td>---</td>
                    <td>---</td>
                    <td>---</td>
                  </tr>
                  <tr>
                    <td>---</td>
                    <td>---</td>
                    <td>---</td>
                  </tr>
                  <tr>
                    <td>---</td>
                    <td>---</td>
                    <td>---</td>
                  </tr>
                  <tr>
                    <td>---</td>
                    <td>---</td>
                    <td>---</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
            
        </div>
      </div>

      <div class="col-md-4">
        <div class="box box-warning">
    
          <div class="box-header with-border">
            <h3 class="box-title">Últimos alumnos registrados</h3>
          </div>

          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>Creación</th>
                    <th>Nombre</th>
                  </tr>
                </thead>
                <tbody>
                  <?php for($i = 0; $i < count($Gd_listaAlumnos); $i++): ?>
                  <tr>
                    <td><?= d($Gd_listaAlumnos[$i]->creacion) ?></td>
                    <td><?= $Gd_listaAlumnos[$i]->nombre ?></td>
                  </tr>
                  <?php endfor; ?>
                </tbody>
              </table>
            </div>
          </div>
            
        </div>
      </div>


      <div class="col-md-4">
        <div class="box box-success">

          <div class="box-header with-border">
            <h3 class="box-title">Últimos pagos registrados</h3>
          </div>

          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>Fecha</th>
                    <th>Nombre</th>
                    <th>Total clases</th>
                  </tr>
                </thead>
                <tbody>
                  <?php for($i = 0; $i < count($Gd_listaPagos); $i++): ?>
                    <tr>
                      <td><?= d($Gd_listaPagos[$i]->fecha); ?></td>
                      <td><?= $Gd_listaPagos[$i]->nombreAlumno." ".$Gd_listaPagos[$i]->paternoAlumno; ?></td>
                      <td><?= $Gd_listaPagos[$i]->monto; ?></td>
                    </tr>
                  <?php endfor; ?>
                </tbody>
              </table>
            </div>
          </div>
            
        </div>
      </div>

    </div>

  </section>

<?php
    break;
  case 3: ?>
  <section class="content-header">
    <h1>
      Bienvenido
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Escritorio</li>
    </ol>
  </section>

<?php
  break;
}
?>

<?php require_once("../required/footer.php");?>
<script type="text/javascript">
$(document).ready(function(){
  <?php switch($_SESSION["Perfil"]){
    case 1: ?>

    var ctx = $("#grafico")[0].getContext("2d");
    var chart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: <?= $Gd_periodos ?>,
        datasets: [{
          label: "Pagos",
          data: <?= $Gd_pagos ?>,
          backgroundColor: 'rgb(54, 162, 235)',
          borderColor: 'rgb(54, 162, 235)',
          fill: false
        },
        {
          label: "Gastos",
          data: <?= $Gd_gastos ?>,
          backgroundColor: 'rgb(255, 205, 86)',
          borderColor: 'rgb(255, 205, 86)',
          fill: false
        }]
      },
      options:{
        legend:{
          display:false
        },
        responsive: true,
        maintainAspectRatio: false,
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true
        }
      }
    });
    <?php break; } ?>
});

</script>
<?php require_once("../required/scripts.php"); ?>
