<?php   
session_start();
header("Content-type: text/html; charset=utf-8");
require_once("../required/functions.php");
require_once("../required/connbd.php");
require_once("perfil.php");

if(isset($_POST) && isset($_POST["nombre"])):
    $obj = new Perfil();

    $obj->id        = $_POST["id"];
    $obj->usuario   = $_SESSION["UserId"];
    $obj->nombre    = $_POST["nombre"];
    $obj->modulos   = $_POST["modulos"];

    $obj->Save();
endif;

if(isset($_POST) && isset($_POST["accion"]) && $_POST["accion"] == "EST"):
    $estado     = $_POST["estado"];
    $nEstado    = "";
    
    if($estado == "A"): 
        $nEstado = "I";
    endif;

    if($estado == "I"):
        $nEstado = "A";
    endif;
    
    $obj            = new Perfil();
    $obj->id        = $_POST["id"];
    $obj->estado    = $nEstado;

    $obj->CambiarEstado();
endif;
?>