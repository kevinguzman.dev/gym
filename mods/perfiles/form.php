<?php
require_once("../required/header.php");
require_once("perfil.php");

$Gd_accion      = "ADD";
$Gd_nombre      = "";
$Gd_perfil      = new Perfil();
$Gd_modulos     = $Gd_perfil->GetModulos();
$Gd_id          = "";

if(isset($_GET["id"]) and $_GET["id"] != "" or $Gd_accion == "GET"):
    $Gd_accion  = "CHG";
    $Gd_id      = $_GET["id"];
    $Gd_obj     = new Perfil();
    $Gd_obj     = $Gd_obj->Get($Gd_id);
    $Gd_nombre  = $Gd_obj->nombre;
endif;
?>

<section class="content-header">
    <h1>
        <?php if($Gd_accion == "ADD"): ?>
            Registrar
        <?php else: ?>
            Modificar
        <?php endif; ?>
        Perfil
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?=$Gl_appUrl?>/perfiles/index">Perfiles</a></li>
        <li class="active">
            <?php if($Gd_accion == "ADD"): ?>
            Registrar
            <?php else: ?>
            Modificar
            <?php endif; ?>
        </li>
    </ol>
</section>

<section class="content">
    <form role="form" action="#" method="POST">
        <input type="hidden" id="id" value="<?=$Gd_id ?>" />
        <div class="row">
            <div class="col-xs-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#gasto" data-toggle="tab">Perfil</a></li>
                        <li class="dropdown pull-right">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            Opciones <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$Gl_appUrl?>/perfiles/form">Registrar nuevo</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$Gl_appUrl?>/perfiles/index">Ver todos</a></li>
                            </ul>
                        </li>
                    </ul>
                    
                    <div class="tab-content">
                        <div class="tab-pane active" id="gasto">
                            
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Ejemplo: profesor, alumno, etc." value="<?= $Gd_nombre ?>">
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Módulos <small>(Clickear para dar acceso)</small></h3>
                    </div>
                    <div class="box-body">
                        <table class="table-responsive no-padding table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Módulo</th>
                                    <th>Acceso</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    for($i = 0; $i < count($Gd_modulos); $i++): ?>
                                        <tr>
                                            <td><?= $Gd_modulos[$i]->id ?></td>
                                            <td><?= $Gd_modulos[$i]->nombre ?></td>
                                            <?php  
                                                #--- solo realizará este ciclo cuando acción sea distinta a "ADD"
                                                $Gd_check = "";
                                                if($Gd_accion != "ADD"):
                                                    #--- logica de check
                                                    for($o = 0; $o < count($Gd_obj->modulos); $o++):
                                                        #--- cuando el
                                                        if($Gd_obj->modulos[$o] == $Gd_modulos[$i]->id):
                                                            $Gd_check = "checked=''";
                                                        endif;
                                                    endfor;
                                                endif;
                                            ?>
                                            <td><input type="checkbox" class="minimal" name="modulos[]" value="<?=$Gd_modulos[$i]->id ?>" <?= $Gd_check ?> /></td>
                                        </tr>
                                <?php   
                                    endfor;?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer" style="padding:10px;">
                    <button type="button" class="btn btn-primary" id="btn"> Guardar</button>
                </div>
            </div>
        </div>
    </form>
</section>

<?php require_once("../required/footer.php"); ?>
<script type="text/javascript">
    $("#btn").click(function(){
        var checkeados      = $("input[name='modulos[]']:checked");
        var modulos         = [];
        var json            = new Object();
        
        if(checkeados.length > 0){
            for(i = 0; i < checkeados.length; i++){
                var id = checkeados[i].value;
                modulos.push(id);
            }
            json["id"]      = $("#id").val();
            json["nombre"]  = $("#nombre").val();
            json["modulos"] = modulos;
            json["accion"]  = "<?= $Gd_accion ?>";
            AlertConfirm("", "Desea guardar este perfil?", function(res){
                if(res){
                    $.ajax({
                        type: "POST",
                        url: "<?= $Gl_appUrl ?>/perfiles/ajax",
                        data: json,
                        success: function(msj){
                            Success("Éxito", "Perfil guardado con éxito");
                        }
                    });
                }
            }, "warning");
        }else{
            AlertError("Error", "Debe seleccionar al menos un permiso.");
        }

    });
</script>
<?php require_once("../required/scripts.php"); ?>