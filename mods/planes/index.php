<?php
  require_once("../required/header.php");
  require_once("plan.php");
  $Gd_obj = new Plan();
  $Gd_fps = json_encode($Gd_obj->GetAll());

  ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Planes
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Planes</li>
  </ol>
</section>

<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Planes registrados</h3>
      <div class="box-tools">
        <a href="<?= $Gl_appUrl ?>/planes/form" class="btn btn-default">Agregar nuevo</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="gastos" class="table table-striped responsive table-hover">
        <thead>
          <th>Código</th>
          <th>Nombre</th>
          <th>Valor</th>
          <th>Acción</th>
        </thead>
      </table>
    </div>
  </div>
</section>

<?php require_once("../required/footer.php");?>
<script type="text/javascript">
$(function () {
$('#gastos').DataTable({
  'language'      : { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json" },
  'paging'        : true,
  'lengthChange'  : true,
  'searching'     : true,
  'ordering'      : true,
  'info'          : true,
  'autoWidth'     : false,
  'responsive'    : true,
  'data'          : <?= $Gd_fps ?>,
  'columns'       : [
                      { data: "id" },
                      { data: "nombre" },
                      { data: "valor" },
                      {
                          sortable: false,
                          className: "table-view-pf-actions",
                          "render": function (data, type, row, meta) {
                            return "<a href='<?=$Gl_appUrl?>/planes/form/"+ row.id +"' class='btn btn-default' title='Editar'><i class='fa  fa-external-link'></i></a>";
                          }
                      },
                    ]
})
})
</script>
<?php require_once("../required/scripts.php"); ?>
