<?php
session_start();
header('Content-type: application/json');
require_once("../required/functions.php");
require_once("../required/connbd.php");
require_once("plan.php");

if(isset($_POST) && isset($_POST["nombre"])):
  $obj          = new Plan();
  $obj->nombre  = $_POST["nombre"];
  $obj->valor   = $_POST["valor"];
  $obj->estado  = $_POST["estado"];
  $obj->id      = $_POST["id"];
  
  echo json_encode($obj->Save());
endif;

 ?>
