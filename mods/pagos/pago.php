<?php

class Pago{
  public $codigo;
  public $alumno;
  public $observaciones;
  public $monto;
  public $responsable;
  public $fecha;
  public $periodo;
  public $formaPago;
  public $planes;
  public $usuario;
  public $motivoEliminacion;
  public $nombreAlumno;
  public $paternoAlumno;

  function __construct(){}

  function Get($cod){
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $sql      = "SELECT * FROM pagos WHERE codpago = ".$cod." LIMIT 1";
    $res      = $strconn->query($sql) or die("Error: ".mysqli_error($strconn));
    $Gd_pago  = new Pago();

    if($res->num_rows >0):
      $row = $res->fetch_assoc();

      $Gd_pago->codigo          = $row["codpago"];
      $Gd_pago->alumno          = $row["codalumno"];
      $Gd_pago->observaciones   = $row["observacion"];
      $Gd_pago->monto           = $row["monto"];
      $Gd_pago->responsable     = $row["usuario"];
      $Gd_pago->fecha           = $row["creacion"];
      $Gd_pago->formaPago       = $row["idformapago"];
    endif;

    $strconn->close();
    return $Gd_pago;
  }

  function Save(){
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $sql  = "INSERT INTO pagos (creacion, monto, observacion, codalumno, usuario, codperiodo, idformapago, estado)";
    $sql .= "             values(now(), ".$this->monto.", '".$this->observaciones."', '".$this->alumno."', '".$this->responsable."', ".$this->periodo.", ".$this->formaPago.", 'A')";

    $strconn->query($sql) or die("error: ".mysqli_error($strconn));

    #--- actualiza los planes asociados al alumno 
    if(count($this->planes) > 0){
      $sql    = "DELETE FROM alumnos_planes WHERE id_alumno = $this->alumno ";
      $strconn->query($sql) or die("error eliminar planes: ".mysqli_error($strconn));

      for($i = 0; $i < count($this->planes); $i++){
        $plan   = $this->planes[$i];
        $sql    = "INSERT INTO alumnos_planes (id_alumno, id_plan, estado) VALUES ('$this->alumno', '$plan', 'A')";
        $strconn->query($sql) or die("error insertar planes: ".mysqli_error($strconn));
      }
    }
    
    //marca al alumno como pagado
    $sql  = "UPDATE alumno SET codultperiodopagado = ".$this->periodo." where codalumno = ".$this->alumno;
    $res  = $strconn->query($sql);

    $strconn->close();
  }

  function GetAll($dashboard = false){
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $sql  = "SELECT t1.codpago, t1.observacion, t1.creacion, t1.usuario, t1.monto, ifnull(t2.nombre, '---') as alumno, t3.nombre as nomUsuario, ifnull(t4.descripcion, '---') as formapago, ifnull(t2.nombres, '---') as nomAlumno, ifnull(t2.appaterno, '---') as patAlumno ";
    $sql .= "FROM pagos t1 ";
    $sql .= "LEFT JOIN alumno t2 ON t1.codalumno = t2.codalumno ";
    $sql .= "INNER JOIN usuarios t3 ON t1.usuario = t3.login ";
    $sql .= "LEFT JOIN formaspago t4 ON t1.idformapago = t4.id ";
    $sql .= "WHERE t1.estado = 'A' ";
    $sql .= "ORDER BY t1.codpago DESC ";

    if($dashboard)
      $sql .= "LIMIT 7 ";

    $res  = $strconn->query($sql) or die("Error:" . mysqli_error($strconn));
    $Gd_array = array();
    if($res->num_rows > 0):
      while($row = $res->fetch_assoc()):
        $Gd_pago = new Pago();

        $Gd_pago->codigo        = $row["codpago"];
        $Gd_pago->alumno        = $row["alumno"];
        $Gd_pago->observaciones = $row["observacion"];
        $Gd_pago->monto         = dinero($row["monto"]);
        $Gd_pago->responsable   = $row["nomUsuario"];
        $Gd_pago->fecha         = dt($row["creacion"]);
        $Gd_pago->formaPago     = $row["formapago"];
        $Gd_pago->usuario       = $row["usuario"];
        $Gd_pago->nombreAlumno  = $row["nomAlumno"];
        $Gd_pago->paternoAlumno = $row["patAlumno"];

        $Gd_array[] = $Gd_pago;
      endwhile;
    endif;
    
    $strconn->close();
    return $Gd_array;
  }

  function GetAlumnos(){
    #--- datos de conexión
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $sql      = "SELECT codalumno, nombre FROM alumno where estado = 'A' ORDER BY appaterno ";
    $res      = $strconn->query($sql);
    $Gd_array = array();

    if($res->num_rows > 0):
      while($row = $res->fetch_assoc()):
        $Gd_codigo  = $row["codalumno"];
        $Gd_nombre  = $row["nombre"];
        $Gd_array[] = array('codigo' => $Gd_codigo, 'nombre' => $Gd_nombre );

      endwhile;
    endif;

    $strconn->close();
    return $Gd_array;
  }

  function GetPeriodos(){
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $sql      = "SELECT codperiodo, nombre from periodos WHERE estado = 'A'";
    $res      = $strconn->query($sql) or die ("Error getPeriodos: " . mysqli_error($strconn));
    $Gd_array = array();

    if($res->num_rows > 0):
      while($row = $res->fetch_assoc()):
        $Gd_periodo = new Periodo();

        $Gd_periodo->codigo = $row["codperiodo"];
        $Gd_periodo->nombre = $row["nombre"];
        $Gd_array[]         = $Gd_periodo;
      endwhile;
    endif;
    $strconn->close();
    return $Gd_array;
  }

  function GetFormaPago(){
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $sql      = "SELECT id, descripcion from formaspago WHERE estado = 'A'";
    $res      = $strconn->query($sql) or die ("Error getPeriodos: " . mysqli_error($strconn));
    $Gd_array = array();

    if($res->num_rows > 0):
      while($row = $res->fetch_assoc()):
        $Gd_fp = new stdClass();

        $Gd_fp->id            = $row["id"];
        $Gd_fp->descripcion   = $row["descripcion"];
        $Gd_array[]           = $Gd_fp;
      endwhile;
    endif;
    $strconn->close();
    return $Gd_array;
  }

      public static function TraerPlanes(){
      #--- variables de conexión
      $conn         = new connbd();
      $strconn      = $conn->connect();
      $Gd_array     = array();

      $sql    = "SELECT id, nombre FROM planes WHERE estado = 'A' ";
      $res    = $strconn->query($sql) or die ("Error planes:" . mysqli_error($strconn));

      if($res->num_rows > 0):
        while($row = $res->fetch_assoc()):
            $obj          = new stdClass();
            $obj->id      = $row["id"];
            $obj->nombre  = $row["nombre"];

            $Gd_array[] = $obj;
        endwhile;
      endif;

      $strconn->close();
      return $Gd_array;
    }


  public static function GetInfoAlumno($idAlumno){
    #--- generar consulta
    #--- $sql = "select join a muchas tablas";

    $conn           = new connbd();
    $strconn        = $conn->connect();
    $Gd_array       = array();

    $sql            = "SELECT t1.id,t1.nombre, t1.valor FROM planes t1 INNER JOIN alumnos_planes t2 ON t1.id = t2.id_plan WHERE t2.id_alumno = '$idAlumno'";
    $res            = $strconn->query($sql) or die ("Error planes:" . mysqli_error($strconn));
      
    if($res->num_rows > 0):
      while($row = $res->fetch_assoc()):
        $obj          = new stdClass();
        $obj->id      = $row["id"];
        $obj->nombre  = $row["nombre"];
        $obj->valor   = $row["valor"];  
        $Gd_array[]   = $obj;
      endwhile;
    endif;

    $strconn->close();
    return $Gd_array;
  }

  #--- obtiene la suma de los planes
  public static function getSumaValorPlanes($planes){
    $conn     = new connbd();
    $strconn  = $conn->connect();
    $suma     = 0;
    $ids      = "";

    #--- recorre el array que contiene los id de los planes
    for($i = 0; $i < count($planes); $i++){
      if($i == 0){
        $ids  = $planes[$i];
      }else{
        $ids  = $ids. "," .$planes[$i];
      } 
    }

    #--- query
    $query        = "SELECT sum(valor) as suma FROM planes WHERE id IN ($ids)";
    $resultado    = $strconn->query($query) or die("Error get suma" . mysqli_error($strconn));

    #--- obtiene los datos
    $fila         = $resultado->fetch_assoc();
    $strconn->close();
    return $fila["suma"];
  }

  #--- elimina un pago
  public function eliminarPago(){
    $conn                     = new connbd();
    $strconn                  = $conn->connect();
    
    $this->motivoEliminacion  = clear($this->motivoEliminacion, $strconn);
    $this->codigo             = clear($this->codigo, $strconn);

    $sql                      = "UPDATE pagos SET estado = 'E', motivo_eliminacion = '". $this->motivoEliminacion ."' WHERE codpago = ".$this->codigo;
    $resultado                = $strconn->query($sql) or die("Error eliminar pago: " . mysqli_error($strconn));

    $strconn->close();
  }
}

 ?>
