<?php
  require_once("../required/header.php");
  require_once("pago.php");

  $Gd_pago    = new Pago();
  $Gd_pagos   = json_encode($Gd_pago->GetAll());
?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Pagos
      <small>Todos</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= $Gl_appUrl ?>/dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Pagos</li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Listado de todos los pagos registrados</h3>
        <div class="box-tools">
          <a href="<?= $Gl_appUrl ?>/pagos/form" class="btn btn-default">Agregar nuevo</a>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="alumnos" class="table table-striped responsive table-hover">
          <thead>
            <th>Fecha</th>
            <th>Alumno</th>
            <th>Monto</th>
            <th>Forma pago</th>
            <th>Responsable</th>
            <th>Acción</th>
          </thead>
        </table>
      </div>
    </div>
  </section>

<?php require_once("../required/footer.php");?>
<script type="text/javascript">
$(function () {
  $('#alumnos').DataTable({
    'language'      : { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json" },
    'paging'        : true,
    'lengthChange'  : true,
    'searching'     : true,
    'ordering'      : false,
    'info'          : true,
    'autoWidth'     : false,
    'responsive'    : true,
    'data'          : <?= $Gd_pagos ?>,
    'columns'       : [
                        { data: "fecha" },
                        { data: "alumno" },
                        { data: "monto" },
                        { data: "formaPago" },
                        { data: "responsable" },
                        {
                            sortable: false,
                            className: "table-view-pf-actions",
                            "render": function (data, type, row, meta) {
                              btn = "<a href='<?=$Gl_appUrl?>/pagos/form/"+ row.codigo +"' class='btn btn-default' title='Editar'><i class='fa  fa-external-link'></i></a> ";
                              
                              if(row.usuario == "<?=$_SESSION["Login"] ?>"){
                                btn += " <button class='btn btn-default' onclick='eliminarPago("+ row.codigo + ")' title='Eliminar pago' ><i class='fa fa-trash-o'></buttom></a>";
                              }
                              
                              return btn;
                            }
                        },
                      ]
  });

  eliminarPago = function(id){
    AlertConfirm("Atención", "Desea eliminar este pago?", function(res){
        
        if(res){
         
          const respuesta = swal({
            title: 'Ingresa un motivo',
            input: 'text',
            inputAttributes: {
              autocapitalize: 'off',
              placeholder: "Debes especificar porqué quieres eliminar este pago",
              required: true,
              id: "textoSwal"
            },
            showCancelButton: true,
            confirmButtonText: 'Aceptar'
          });

          //var motivo = $("#textoSwal").val();
          respuesta.then(function(value){
            Load("Cargando...");

            var json          = new Object();
            json["pago"]      = id;
            json["msj"]       = value;
            json["accion"]    = "DEL";
            
            $.ajax({
              url: '<?=$Gl_appUrl?>/pagos/ajax',
              type: 'post',
              dataType: 'json',
              data: json,
              success: function(data) {
                AlertSuccess("Éxito", "Pago eliminado con éxito", '<?=$Gl_appUrl?>/pagos/index')
              },
              error: function(data){
                console.log(data);
                AlertError("Error", data.responseText);
              }
            });
          
          });
        }
    }, "warning");
    
  }
});
</script>
<?php require_once("../required/scripts.php"); ?>
