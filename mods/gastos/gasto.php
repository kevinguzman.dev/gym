<?php
class Gasto{
  public $codigo;
  public $periodo;
  public $descripcion;
  public $monto;
  public $nomPeriodo;
  public $responsable;
  public $fecha;

  function __construct(){}

  function Save(){
    $conn        = new connbd();
    $strconn     = $conn->connect();
    $Gd_monto    = $this->monto;
    if($this->Exists()):
      #--- actualiza
      $sql = "UPDATE gastos SET ";
      $sql .= " descripcion = '".$this->descripcion."', ";
      $sql .= " monto       = '".$Gd_monto."', ";
      $sql .= " fecha       = NOW() ";
      $sql .= " WHERE codgasto = ".$this->codigo;

      $res = $strconn->query($sql) or die("error update: ".mysqli_error($strconn));
    else:
      #--- agrega
      $sql  = "INSERT INTO gastos (descripcion, monto, codperiodo, responsable, fecha)";
      $sql .= "values ('". $this->descripcion ."', '".$Gd_monto."', ".$this->periodo.", '".$this->responsable."', now())";
      $res  =  $strconn->query($sql) or die ("Error: ". mysqli_error($strconn));
      $this->codigo = $strconn->insert_id;
    endif;
    $strconn->close();
  }

  function Exists(){
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $sql = "SELECT count(codgasto) as count FROM gastos WHERE codgasto = ".$this->codigo." LIMIT 1";
    $res = $strconn->query($sql) or die("Error: ". mysqli_error($strconn));

    if($res->num_rows > 0):
      $row = $res->fetch_assoc();
      if($row["count"] > 0):
        return true;
      else:
        return false;
      endif;
    else:
      return false;
    endif;
  }

  function Get($cod){
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $sql    = "SELECT codgasto, descripcion, monto, codperiodo, responsable, fecha FROM gastos WHERE codgasto = ".$cod;
    $res    = $strconn->query($sql) or die ("Error get: ".mysqli_error($strconn) );
    $Gd_obj = new Gasto();

    if($res->num_rows > 0):
      $row    = $res->fetch_assoc();
      $Gd_obj->codigo       = $row["codgasto"];
      $Gd_obj->periodo      = $row["codperiodo"];
      $Gd_obj->descripcion  = $row["descripcion"];
      $Gd_obj->monto        = miles($row["monto"]);
      $Gd_obj->fecha        = dt($row["fecha"]);
      $Gd_obj->responsable  = $row["responsable"];
    endif;

    $strconn->close();

    return $Gd_obj;
  }

  function GetPeriodos(){
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $sql      = "SELECT codperiodo, nombre from periodos WHERE estado = 'A'";
    $res      = $strconn->query($sql) or die ("Error getPeriodos: " . mysqli_error($strconn));
    $Gd_array = array();

    if($res->num_rows > 0):
      while($row = $res->fetch_assoc()):
        $Gd_periodo = new Periodo();

        $Gd_periodo->codigo = $row["codperiodo"];
        $Gd_periodo->nombre = $row["nombre"];
        $Gd_array[]         = $Gd_periodo;
      endwhile;
    endif;
    $strconn->close();
    return $Gd_array;
  }

  function GetAll(){
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $sql  = "select codgasto, monto, t2.nombre, t1.responsable, t1.fecha, t3.nombre as responsable ";
    $sql .= "from gastos t1 ";
    $sql .= "inner join periodos t2 on t1.codperiodo = t2.codperiodo ";
    $sql .= "inner join usuarios t3 ON t1.responsable = t3.login ";
    $sql .= "ORDER BY codgasto DESC ";

    $res = $strconn->query($sql) or die("Error: " . mysqli_error($strconn));
    $Gd_array = array();

    if($res->num_rows > 0):
      while($row = $res->fetch_assoc()):
          $Gd_gas = new Gasto();
          $Gd_gas->codigo       = $row["codgasto"];
          $Gd_gas->monto        = dinero($row["monto"]);
          $Gd_gas->nomPeriodo   = $row["nombre"];
          $Gd_gas->responsable  = $row["responsable"];
          $Gd_gas->fecha        = dt($row["fecha"]);

          $Gd_array[]         = $Gd_gas;
      endwhile;
    endif;

    $strconn->close();
    return $Gd_array;
  }
}


?>
