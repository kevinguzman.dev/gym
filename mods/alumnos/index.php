<?php
  require_once("../required/header.php");
  require_once("alumno.php");

  $Gd_alumno  = new Alumno();
  $Gd_alumnos = json_encode($Gd_alumno->GetAll());
?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Alumnos
      <small>Todos</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Alumnos</li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Listado de todos los alumnos registrados</h3>
        <div class="box-tools">
          <a href="<?=$Gl_appUrl?>/alumnos/form" class="btn btn-default">Agregar nuevo</a>
        </div>
      </div>

      <!-- /.box-header -->
      <div class="box-body">
        <table id="alumnos" class="table table-striped responsive table-hover">
          <thead>
            <th>Creación</th>
            <th>Rut</th>
            <th>Nombre</th>
            <th>Fecha nacimiento</th>
            <th>Celular</th>
            <th>Acción</th>
          </thead>
        </table>
      </div>
    </div>
  </section>

<?php require_once("../required/footer.php");?>
<script type="text/javascript">
$(function () {
  $('#alumnos').DataTable({
    'language'      : { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json" },
    'paging'        : true,
    'lengthChange'  : true,
    'searching'     : true,
    'ordering'      : true,
    'info'          : true,
    'autoWidth'     : false,
    'data'          : <?= $Gd_alumnos ?>,
    'responsive'    : true,
    'columns'       : [
                        { data: "creacion" },
                        { data: "rut" },
                        { data: "nombre" },
                        { data: "fNacimiento" },
                        { data: "celular" },
                        {
                            sortable: false,
                            className: "table-view-pf-actions",
                            "render": function (data, type, row, meta) {
                              return "<a href='<?=$Gl_appUrl?>/alumnos/form/"+ row.codigo +"' class='btn btn-default' title='Editar'><i class='fa  fa-external-link'></i></a>";
                            }
                        },
                      ]
  })
})
</script>
<?php require_once("../required/scripts.php"); ?>
<!--"<button> " + row.codigo + "</button"-->
