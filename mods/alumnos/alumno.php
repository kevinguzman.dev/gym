<?php
class alumno{
    public $creacion;
    public $nombre;
    public $paterno;
    public $materno;
    public $codigo;
    public $rut;
    public $fNacimiento;
    public $estado;
    public $celular;
    public $telefono;
    public $email;
    public $accion;
    public $bjj;
    public $planes;
    public $nomusuario;
    public $contrasena;
    public $becado;
    public $nombres;

    function __construct(){}

    function getNombreCompleto(){
      return $this->nombre." ".$this->paterno." ".$this->materno;
    }

    function Save(){
      #--- datos de conexión
      $conn        = new connbd();
      $strconn     = $conn->connect();
      $nombre      = $this->nombre." ".$this->paterno." ".$this->materno;
      #--- inserta/Modifica un alumno
      if($this->Exists()):
        #--- cuando existe el alumno
        $sql  = "UPDATE alumno ";
        $sql  .="SET";
        $sql  .="   nombres             = '".$this->nombre."', ";
        $sql  .="   appaterno           = '".$this->paterno."', ";
        $sql  .="   apmaterno           = '".$this->materno."', ";
        $sql  .="   nombre              = '".$nombre."', ";
        $sql  .="   rut                 = '".$this->rut."', ";
        $sql  .="   fechanacimiento     = '".$this->fNacimiento."', ";
        $sql  .="   estado              = '".$this->estado."', ";
        $sql  .="   celular             = '".$this->celular."', ";
        $sql  .="   telefono            = '".$this->telefono."', ";
        $sql  .="   email               = '".$this->email."', ";
        $sql  .="   becado               = ".$this->becado." ";
        $sql  .="WHERE codalumno = ".$this->codigo;
         
        $strconn->query($sql) or die("error update: ". mysqli_error($strconn));
      else:
        #--- cuando no existe el alumno
        $sql   = "INSERT INTO alumno (creacion, nombre, rut, fechanacimiento, estado, celular, telefono, email, nombres, appaterno, apmaterno, becado, codperiodo)";
        $sql  .= "              values(now(), '".$nombre."', '".$this->rut."', '".$this->fNacimiento."', '".$this->estado."', '".$this->celular."', '".$this->telefono."', '".$this->email."', '".$this->nombre."', '".$this->paterno."', '".$this->materno."', ". $this->becado .", (SELECT codperiodo FROM periodos WHERE estado = 'A' LIMIT 1)  )";

        $strconn->query($sql) or die("error insert alumno: ". mysqli_error($strconn));
        $this->codigo = $strconn->insert_id;
      endif;

      $this->SavePlanes();
      $strconn->close();
    }

    function Exists(){
      $conn        = new connbd();
      $strconn     = $conn->connect();

      $sql        = "select codalumno from alumno where rut = '".$this->rut. "' LIMIT 1";
      $resultado  = $strconn->query($sql) or die("Error exists: ". mysqli_error($strconn));

      if($resultado->num_rows > 0):
        $row            = $resultado->fetch_array();
        $this->codigo   = $row["codalumno"];

        return true;
      else:
        return false;
      endif;
    }

    function Get($cod){
      #--- variables de conexión
      $conn         = new connbd();
      $strconn      = $conn->connect();

      #--- obtiene un alumno
      $sql = "SELECT codalumno, creacion, nombres, appaterno, apmaterno, rut, fechanacimiento, estado, celular, telefono, email, becado FROM alumno WHERE codalumno = ".$cod." LIMIT 1";
      $res = $strconn->query($sql) or die("consulta: " . mysqli_error($strconn));
      
      $Gd_alumno = new Alumno();

      if($res->num_rows > 0):
        $row = $res->fetch_assoc();

        $Gd_alumno->codigo      = $row["codalumno"];
        $Gd_alumno->creacion    = date("d-m-Y HH:mm:ss", strtotime($row["creacion"]));
        $Gd_alumno->nombre      = $row["nombres"];
        $Gd_alumno->paterno     = $row["appaterno"];
        $Gd_alumno->materno     = $row["apmaterno"];
        $Gd_alumno->rut         = $row["rut"];
        $Gd_alumno->fNacimiento = date("d-m-Y", strtotime($row["fechanacimiento"]));
        $Gd_alumno->estado      = $row["estado"];
        $Gd_alumno->celular     = $row["celular"];
        $Gd_alumno->telefono    = $row["telefono"];
        $Gd_alumno->email       = $row["email"];
        $Gd_alumno->planes      = alumno::GetPlanes($Gd_alumno->codigo);
        $Gd_alumno->becado      = $row["becado"];
      else:
        $Gd_alumno = null;
      endif;
      $strconn->close();

      return $Gd_alumno;
    }

    private static function GetPlanes($idAlumno){
      #--- variables de conexión
      $conn         = new connbd();
      $strconn      = $conn->connect();
      $Gd_array     = array();
      
      $sql = "SELECT id_plan FROM alumnos_planes WHERE id_alumno = ".$idAlumno;
      $res = $strconn->query($sql) or die("Error getalumnos" . mysqli_error($strconn));

      while($row = $res->fetch_array()):
        $Gd_array[] = $row["id_plan"];
      endwhile;

      $strconn->close();
      return $Gd_array;
    }

    function GetAll($dashboard = false){
      #--- variables de conexión
      $conn         = new connbd();
      $strconn      = $conn->connect();
      $Gd_array     = array();

      #--- obtiene todos los alumnos registrados
      $sql        = "SELECT codalumno, creacion, nombre, rut, fechanacimiento, estado, celular, telefono, email, ifnull(nombres, '---') as nomAlumno, ifnull(apmaterno, '---') as materno, ifnull(appaterno, '---') as paterno   ";
      $sql       .= "FROM alumno ";
      $sql       .= "ORDER BY codalumno DESC ";

      if($dashboard)
        $sql       .= "LIMIT 7"; 

      $resultado  = $strconn->query($sql) or die("error: ". mysqli_error($strconn));
      
      if($resultado->num_rows > 0):
        while($row = $resultado->fetch_assoc()):
          $Gd_alumno = new Alumno();

          $Gd_alumno->codigo      = $row["codalumno"];
          $Gd_alumno->creacion    = dt($row["creacion"]);
          $Gd_alumno->nombre      = $row["nombre"];
          $Gd_alumno->rut         = $row["rut"];
          $Gd_alumno->fNacimiento = date("d-m-Y", strtotime($row["fechanacimiento"]));
          $Gd_alumno->estado      = $row["estado"];
          $Gd_alumno->celular     = $row["celular"];
          $Gd_alumno->telefono    = $row["telefono"];
          $Gd_alumno->email       = $row["email"];
          $Gd_alumno->paterno     = $row["paterno"];
          $Gd_alumno->materno     = $row["materno"];
          $Gd_alumno->nombres     = $row["nomAlumno"];

          $Gd_array[] = $Gd_alumno;
        endwhile;
      endif;
      $strconn->close();

      return $Gd_array;
    }

    public static function TraerPlanes(){
      #--- variables de conexión
      $conn         = new connbd();
      $strconn      = $conn->connect();
      $Gd_array     = array();

      $sql    = "SELECT id, nombre FROM planes WHERE estado = 'A'";
      $res    = $strconn->query($sql) or die ("Error planes:" . mysqli_error($strconn));

      if($res->num_rows > 0):
        while($row = $res->fetch_assoc()):
            $obj          = new stdClass();
            $obj->id      = $row["id"];
            $obj->nombre  = $row["nombre"];

            $Gd_array[] = $obj;
        endwhile;
      endif;

      $strconn->close();
      return $Gd_array;
    }

    public function SavePlanes(){
      #--- variables de conexión
      $conn       = new connbd();
      $strconn    = $conn->connect();
      
      $sql        = "DELETE FROM alumnos_planes WHERE id_alumno = ".$this->codigo;
      $strconn->query($sql) or die("Error delete: " . mysqli_error($strconn));

      for($i = 0; $i < count($this->planes); $i++):
        $sql = "INSERT INTO alumnos_planes (id_alumno, id_plan, estado) VALUES (".$this->codigo.", ".$this->planes[$i].", 'A')";
        $strconn->query($sql) or die("Error insert plan: " . mysqli_error($strconn));
      endfor;

      $strconn->close();
    }

    public function ExisteUsuario(){
      #--- variables de conexión
      $conn       = new connbd();
      $strconn    = $conn->connect();

      $sql        = "SELECT ifnull(codusuario, '-') as codusuario FROM usuarios WHERE id_alumno = $this->codigo";
      $res        = $strconn->query($sql) or die ("Error traer usuario: " . mysqli_error($strconn));
      $fila       = $res->fetch_array();
      
      if( is_null($fila["codusuario"]) ):
        return false;
      else:
        return true;
      endif;
    }

    public function GuardarUsuario(){
      $this->contrasena     = generaString(8); 
      $nombreCompleto       = $this->getNombreCompleto();
      $this->nomusuario     = generaNombreUsuario($this->nombre, $this->paterno);

      #--- variables de conexión
      $conn                 = new connbd();
      $strconn              = $conn->connect();

      $sql                  = "INSERT INTO usuarios (creacion, password, nombre, email, estado, foto, login, id_perfil, id_alumno)
                                    VALUES(now(), '$this->contrasena', '$nombreCompleto', '$this->email', 'A', 'no-photo.png', '$this->nomusuario' , 3, '$this->codigo')";
      
      $strconn->query($sql) or die("Error insert usuario: " . mysqli_error($strconn));


    }
}

?>
