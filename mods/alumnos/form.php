<?php
require_once("../required/header.php");
require_once("alumno.php");
#------------------
#---- Variables----
#------------------
$Gd_nombre      = "";
$Gd_paterno     = "";
$Gd_materno     = "";
$Gd_codigo      = 0;
$Gd_rut         = "";
$Gd_fNacimiento = date("d-m-Y");
$Gd_estado      = "A";
$Gd_celular     = "";
$Gd_telefono    = "";
$Gd_email       = "";
$Gd_accion      = "ADD";
$Gd_exito       = false;
$Gd_bjj         = "";
$Gd_planes      = alumno::TraerPlanes();
$Gd_planAlumno  = array();
$Gd_becado      = "";

if( isset($_GET["id"]) and $_GET["id"] >0):
  $Gd_accion      = "GET";
  $Gd_codigo      = $_GET["id"];

  $Gd_alumno      = new Alumno();
  $Gd_alumno      = $Gd_alumno->Get($Gd_codigo);

  $Gd_nombre      = $Gd_alumno->nombre;
  $Gd_paterno     = $Gd_alumno->paterno;
  $Gd_materno     = $Gd_alumno->materno;
  $Gd_codigo      = $Gd_alumno->codigo;
  $Gd_rut         = $Gd_alumno->rut;
  $Gd_fNacimiento = $Gd_alumno->fNacimiento;
  $Gd_estado      = $Gd_alumno->estado;
  $Gd_telefono    = $Gd_alumno->telefono;
  $Gd_email       = $Gd_alumno->email;
  $Gd_celular     = $Gd_alumno->celular;
  $Gd_planAlumno  = $Gd_alumno->planes;
  
  if($Gd_alumno->becado == 1):
    $Gd_becado = "checked='true'";
  endif;
else:
  $Gd_codigo      = 0;
endif;
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php if($Gd_accion == "ADD"): ?>
          Crear
        <?php else: ?>
          Modificar
        <?php endif; ?>
        Alumno
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?=$Gl_appUrl?>/alumnos/index">Alumnos</a></li>
        <li class="active">
          <?php if($Gd_accion == "ADD"): ?>
            Crear
          <?php else: ?>
            Modificar
          <?php endif; ?>
        </li>
      </ol>
    </section>

    <section class="content">

      <div class="row">
        <div class="col-xs-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#alumno" data-toggle="tab">Alumno</a></li>
              <li class="dropdown pull-right">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  Opciones <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= $Gl_appUrl ?>/alumnos/index">Ver todos</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= $Gl_appUrl ?>/alumnos/form">Registrar nuevo</a></li>
                  <li role="presentation" class="divider"></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= $Gl_appUrl ?>/pagos/form">Registrar pago</a></li>
                </ul>
              </li>
            </ul>

            <div class="tab-content">
              <div class="tab-pane active" id="alumno">
                <form role="form" method="POST">
                  <input type="hidden" name="codigo" value="<?= $Gd_codigo ?>">

                  <div class="box-body">
                    <div class="form-group">
                      <label for="rut">Rut</label>
                      <input type="text" class="form-control" id="rut" name="rut" placeholder="Ingrese rut" value="<?= $Gd_rut  ?>" data-inputmask='"mask": "99.999.999-*"' data-mask required="" autocomplete="off">
                    </div>

                    <div class="form-group">
                      <label for="Nombre">Nombres</label>
                      <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingrese nombres del alumno" value="<?= $Gd_nombre ?>" autocomplete="off">
                    </div>

                    <div class="form-group">
                      <label for="paterno">Paterno</label>
                      <input type="text" name="nombre" class="form-control" id="paterno" placeholder="Ingrese apellido paterno" value="<?= $Gd_paterno ?>" autocomplete="off">
                    </div>

                    <div class="form-group">
                      <label for="materno">Materno</label>
                      <input type="text" name="materno" class="form-control" id="materno" placeholder="Ingrese apellido materno" value="<?= $Gd_materno ?>" autocomplete="off">
                    </div>

                    <div class="form-group">
                       <label for="datepicker">Fecha de nacimiento:</label>
                       <div class="input-group date">
                         <div class="input-group-addon">
                           <i class="fa fa-calendar"></i>
                         </div>
                         <input type="text" name="nacimiento" class="form-control pull-right" id="datepicker" placeholder="Ingrese fecha de nacimiento" value="<?= $Gd_fNacimiento ?>" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                       </div>
                     </div>

                    <div class="form-group">
                      <label for="celular">Celular</label>
                      <input type="text" class="form-control" id="celular" name="celular" placeholder="Ingrese celular" value="<?= $Gd_celular ?>" data-inputmask='"mask": "(+569) 99999999"' data-mask>
                    </div>

                    <div class="form-group">
                      <label for="email">Email</label>
                      <input type="text" class="form-control" id="email" name="email" placeholder="Ingrese email" value="<?= $Gd_email ?>" autocomplete="off">
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <label>Estado</label>
                        <select name="estado" id="estado" class="form-control">
                          <option value="A">Activo</option>
                          <option value="I">Inactivo</option>
                          <option value="E">Eliminado</option>
                        </select>
                      </div>

                      <div class="col-md-6 form-group dvPlan">
                        <label for="plan">Plan/es</label>
                        <select name="plan[]" id="plan" class="form-control select2" multiple="multiple" data-placeholder="Seleccione uno o más planes" style="width: 100%;" tabindex="0">
                          <?php 
                            for($i = 0; $i < count($Gd_planes); $i++ ):
                              $Gd_select = "";
                              for($e = 0 ; $e < count($Gd_planAlumno); $e++):
                                if($Gd_planes[$i]->id == $Gd_planAlumno[$e]):
                                  $Gd_select = "selected = ''";
                                  break;
                                endif;
                              endfor;
                            ?>
                            <option value="<?= $Gd_planes[$i]->id ?>" <?= $Gd_select ?>><?= $Gd_planes[$i]->nombre ?></option>
                          <?php endfor; ?>
                        </select>
                        <span class="help-block" id="msjError" style="display:none;">Seleccione uno o más planes</span>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6 form-group">
                        <div class="form-group">
                          <label for="becado">Becado &nbsp &nbsp &nbsp <input type="checkbox" class="minimal" id="becado" name="becado" <?=$Gd_becado?> /></label>
                          
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 form-group">
                      <!--
                      <div class="form-group">
                        <label for="foto">Foto</label>
                        <input type="file" class="form-control" id="foto" name="foto" >
                      </div>-->

                    </div>

                  </div>

                  <div class="box-footer">
                    <button type="button" class="btn btn-primary" id="btn">Guardar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>


        </div>
      </div>
    </section>
<?php require_once("../required/footer.php"); ?>
<script type="text/javascript">
$(function(){
  $(".select2").select2();
  $('#datepicker').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
  $('[data-mask]').inputmask();

  Validar = function(){
    var valor   = $("#plan").val();
    var becado  = $("#becado").prop('checked');
    
    if(!becado){
      if(valor.length === 0){
        $(".dvPlan").addClass("has-error");
        $("#msjError").removeAttr("style");
        $("#btn").attr("disabled", "disabled");
        return false;
      }else{
        $("#msjError").hide();
        $(".dvPlan").removeClass("has-error");
        $("#btn").removeAttr("disabled");
        return true;
      }
    }else{
      return true;
    }

  }

  $("#btn").click(function(){
    if(Validar()){
      AlertConfirm("Confirmación", "Desea guardar los cambios realizados?", function(res){
        if(res){
          Load("Cargando...");
          var json            = new Object();
          json["codigo"]      = <?= $Gd_codigo ?>;
          json["nombre"]      = $("#nombre").val();
          json["paterno"]     = $("#paterno").val();
          json["materno"]     = $("#materno").val();
          json["nacimiento"]  = $("#datepicker").val();
          json["planes"]      = $("#plan").val();
          json["rut"]         = $("#rut").val();
          json["celular"]     = $("#celular").val();
          json["estado"]      = $("#estado").val();
          json["email"]       = $("#email").val();
          //json["foto"]        = $("#foto");
          json["becado"]      = $("#becado").val();
          json["accion"]      = "ALU";

          $.ajax({
            url: "<?=$Gl_appUrl?>/alumnos/ajax",
            type: 'POST',
            dataType: 'json',
            data: json,
            success: function(data) {
              Success("Éxito", "Alumno guardado con éxito");

              if(data.resultado == "OK-N"){
                var codigo = data.codigo;
                GuardarUsuario(codigo);
              }

            },
            error: function(data){
              AlertError("Error", data.responseText);
            }
          });
      }
    }, "warning");
    }
  });

  function GuardarUsuario(codigo){
    AlertConfirm("Guardar como usuario", "Desea registrar a este alumno como usuario del sistema?", function(res){
      if(res){
        var json          = new Object();
        json["accion"]    = "USR";
        json["codigo"]    = codigo;
        json["nombre"]    = $("#nombre").val();
        json["paterno"]   = $("#paterno").val();
        json["materno"]   = $("#materno").val();
        json["email"]     = $("#email").val();

        $.ajax({
          url: "<?=$Gl_appUrl?>/alumnos/ajax",
          type: 'POST',
          dataType: 'json',
          data: json,
          beforeSend: function(){
            Load("Cargando...");
          },
          success: function(data){
            Success("Éxito", "Se ha registrado al alumno como usuario, se ha enviado un correo con sus credenciales");
          },
          error: function(data){
            AlertError("Error", data.responseText);
          }
        });
      }
    }, 'warning');
  }


  $("#plan").change(function(){
    $("#btn").removeAttr("disabled");
    $("#msjError").hide();
    $(".dvPlan").removeClass("has-error");
    $("#btn").removeAttr("disabled");
  });

  $("#becado").click(function(){
    console.log("asdas");

    if($("#becado").prop("checked")){
      $("#plan").prop("disabled", true);
      console.log("paso");
    }else{
      $("#plan").prop("disabled", false);
      console.log("no paso");
    }
    
  });
});
</script>
<?php require_once("../required/scripts.php"); ?>
